export interface GetDocumentsDto {
  pageNumber: number;
  pageLength: number;
  total: number;
  
}