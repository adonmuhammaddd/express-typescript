import { mockRequest, mockResponse } from "../__mocks__"
import { getUsers, getUserById, createUser } from "../handlers/usersHandler"

describe('getUsers', () => {
  it('should retun an array of users', () => {
    getUsers(mockRequest, mockResponse);
    expect(mockResponse.send).toHaveBeenCalledWith([])
  });
});
describe('getUserById', () => {
  it('should retun an object of a user', () => {
    getUserById(mockRequest, mockResponse);
    expect(mockResponse.send).toHaveBeenCalledWith({})
  });
});
describe('createUser', () => {
  it('should retun an object of a created user', () => {
    createUser(mockRequest, mockResponse);
    expect(mockResponse.send).toHaveBeenCalledWith({})
  });
});