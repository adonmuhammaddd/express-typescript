import express from 'express'
import usersRouter from './routes/usersRouter'

export function createApp() {
  const app = express()

  app.use("/api/users/", usersRouter)

  return app
}