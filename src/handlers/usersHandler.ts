import { CreateUserDto } from "@/dtos/CreateUser.dto";
import { CreateUserQueryParams } from "@/types/query-params";
import { UserResponse } from "@/types/response";
import { Request, Response } from "express-serve-static-core";

export function getUsers(request: Request, response: Response) {
  response.send([])

}
export function getUserById(request: Request, response: Response) {
  response.send({})

}
export function createUser(request: Request<{}, {}, CreateUserDto, CreateUserQueryParams>, response: Response<UserResponse>) {
  
  response.status(201).send({
    id: 123,
    username: "adon",
    email: "adon@dev.com"
  })

}