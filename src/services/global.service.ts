export function sliceContentBetween(text: string, startText: string, endText: string) {
  const startIndex = text.indexOf(startText);
  if (startIndex === -1) {
      throw new Error(`Start text "${startText}" not found in the provided text.`);
  }
  const endIndex = text.indexOf(endText, startIndex + startText.length);
  if (endIndex === -1) {
      throw new Error(`End text "${endText}" not found in the provided text.`);
  }

  const contentStartIndex = startIndex + startText.length
  const slicedContent = text.substring(contentStartIndex, endIndex)

  return slicedContent;
}