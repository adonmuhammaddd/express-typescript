import express from 'express'
import path from 'path'
import documentRoutes from './documentsRoutes'

const router = express.Router()

router.get("/", async function (req, res) {
  res.sendFile(path.join(__dirname, '/assets/index.html'));
});

const RouteList = [
  {
    path: '/documents',
    route: documentRoutes
  }
]

RouteList.forEach((route) => {
  try {
    router.use(route.path, route.route)
  } catch (error) {
    console.log(error)
  }
})

export default router